package com.bigc.pfs.utils;

public class Constants {

//	public static final int ITEM_PER_PAGE = 4;
	
	public static final int REST_TIMEOUT_CONNECTION = 5000;			// Milliseconds
	public static final int REST_TIMEOUT_SOCKET = 7000;				// Milliseconds
	
	public static final String RS_RESERVE = "reserv";
	public static final String RS_SCAN = "scan";
	public static final String RS_SAVE = "save";
	public static final String RS_SETTING = "setting";
	public static final String RS_DELETE = "delete";
	public static final String RS_CLEAR = "clear";
	
	public static final String SHORTCUT_MEMBER = "m";
	public static final String SHORTCUT_FINISH = "f";
	public static final String SHORTCUT_QNT = "*";
	
	public static final String PR_TYPE_FRESHFOOD = "F";
	
//	public static final String SCAN_ERROR_TITLE = "ไม่สามารถเชื่อมต่อเซิร์ฟเวอร์ได้";
//	public static final String SCAN_ERROR_MSG = "กรุณาตรวจสอบ \n  - สถานะไวไฟ \n  - สถานะเครื่องเซิร์ฟเวอร์  \n  - การตั้งค่าโปรแกรม";
	
//	public static final String SCAN_NOTFOUND_TITLE = "ไม่พบข้อมูลรหัสสินค้า";
//	public static final String SCAN_NOTFOUND_MSG = "กรุณาตรวจสอบ \n  - รหัสสินค้า และลองใหม่อีกครั้ง";
	public static final String SCAN_NOTFOUND_MSG = "กรุณาตรวจสอบใหม่อีกครั้ง";
	
	public static final String CONNECTION_ERROR_TITLE = "ไม่สามารถเชื่อมต่อเซิร์ฟเวอร์ได้";
//	public static final String CONNECTION_ERROR_MSG = "กรุณาตรวจสอบ \n  - สถานะไวไฟ \n  - สถานะเครื่องเซิร์ฟเวอร์  \n  - การตั้งค่าโปรแกรม";		// not use -> use e.toString() instead
	
//	public static final String RESERVE_ERROR_TITLE = "ไม่สามารถเชื่อมต่อเซิร์ฟเวอร์ได้";
//	public static final String RESERVE_ERROR_MSG = "กรุณาตรวจสอบ \n  - สถานะไวไฟ \n  - สถานะเครื่องเซิร์ฟเวอร์  \n  - การตั้งค่าโปรแกรม";
	
	public static final String RESERVE_REQUIRE_TITLE = "ต้องโหลดเลขที่ใหม่ก่อน";
	public static final String RESERVE_REQUIRE_MSG = "กรุณาโหลดเลขที่ใหม่ก่อนทำการสแกนสินค้า  \n  - กดปุ่ม \"โหลดเลขที่ใหม่\" ขวาบน";
	
//	public static final String SAVE_ERROR_TITLE = "ไม่สามารถเชื่อมต่อเซิร์ฟเวอร์ได้";
//	public static final String SAVE_ERROR_MSG = "กรุณาตรวจสอบ \n  - สถานะไวไฟ \n  - สถานะเครื่องเซิร์ฟเวอร์  \n  - การตั้งค่าโปรแกรม";
	
	public static final String SERVERERROR_TITLE = "เกิดข้อผิดพลาดที่เซิร์ฟเวอร์";
	public static final String SERVERERROR_MSG = "เกิดข้อผิดพลาดที่เซิร์ฟเวอร์ กรุณาลองใหม่อีกครั้ง";
	
	public static final String NOSALE_TITLE = "จำกัดจำนวนซื้อ";
	
	public static final String MEMBER_INCORRECT_TITLE = "รหัสสมาชิกไม่ถูกต้อง";
	public static final String MEMBER_INCORRECT_MSG = "กรุณาตรวจสอบ \n  - รหัสสมาชิก และลองใหม่อีกครั้ง";
	
}
