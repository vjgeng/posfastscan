package com.bigc.pfs.utils;

public class AppPreferences {

	public static final String USERCODE = "usercode";
	public static final String PASSWORD = "password";
	public static final String DEVICE_NO = "deviceNo";
//	public static final String STORE_CODE = "storeCode";
	public static final String SERVER_IP = "serverIp";
	public static final String SERVER_PORT = "serverPort";
	
	public static final String DEFAULT_USERCODE = "";
	public static final String DEFAULT_PASSWORD = "";
	public static final String DEFAULT_DEVICE_NO = "1";
//	public static final String DEFAULT_STORE_CODE = "999";
	public static final String DEFAULT_SERVER_IP = "10.4.7.162";	// YYWPOS
//	public static final String DEFAULT_SERVER_IP = "127.0.0.1";
	public static final String DEFAULT_SERVER_PORT = "8000";
	
}
