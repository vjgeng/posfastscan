package com.bigc.pfs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.bigc.pfs.activity.R;
import com.bigc.pfs.activity.ScanActivity;
import com.bigc.pfs.utils.AppPreferences;

public class SettingDialogFragment extends DialogFragment {
	
	private static SettingDialogFragment dialog = null;
	private SharedPreferences appPreferences;
	
	private EditText edtUsercode;
	private EditText edtPassword;
	private EditText edtDeviceNo;
//	private EditText edtStoreCode;
	private EditText edtServerIp;
	private EditText edtServerPort;
	
	private TextView txtError;
	
	public EditText getEdtUsercode() {
		return edtUsercode;
	}
	public EditText getEdtPassword() {
		return edtPassword;
	}
	public EditText getEdtDeviceNo() {
		return edtDeviceNo;
	}
//	public EditText getEdtStoreCode() {
//		return edtStoreCode;
//	}
	public EditText getEdtServerIp() {
		return edtServerIp;
	}
	public EditText getEdtServerPort() {
		return edtServerPort;
	}
	
	public TextView getTxtError() {
		return txtError;
	}

	private SettingDialogFragment(SharedPreferences appPreferences) {
		this.appPreferences = appPreferences;
	}
	
	public static SettingDialogFragment getInstance(SharedPreferences appPreferences) {
		
		if (dialog == null) {
			dialog = new SettingDialogFragment(appPreferences);
		}
		
		return dialog;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		final View view = inflater.inflate(R.layout.dialog_setting, null);
		
		edtUsercode = (EditText) view.findViewById(R.id.usercode);
		edtUsercode.setText(appPreferences.getString(AppPreferences.USERCODE, AppPreferences.DEFAULT_USERCODE));
		edtUsercode.setTypeface(ScanActivity.typeThaiSans);
		
		edtPassword = (EditText) view.findViewById(R.id.password);
		edtPassword.setText(appPreferences.getString(AppPreferences.PASSWORD, AppPreferences.DEFAULT_PASSWORD));
		edtPassword.setTypeface(ScanActivity.typeThaiSans);
		
		edtDeviceNo = (EditText) view.findViewById(R.id.device_no);
		edtDeviceNo.setText(appPreferences.getString(AppPreferences.DEVICE_NO, AppPreferences.DEFAULT_DEVICE_NO));
		edtDeviceNo.setTypeface(ScanActivity.typeThaiSans);
		
//		edtStoreCode = (EditText) view.findViewById(R.id.store_code);
//		edtStoreCode.setText(appPreferences.getString(AppPreferences.STORE_CODE, AppPreferences.DEFAULT_STORE_CODE));
//		edtStoreCode.setTypeface(ScanActivity.typeThaiSans);
//		edtStoreCode.requestFocus(View.FOCUS_RIGHT);
		
		edtServerIp = (EditText) view.findViewById(R.id.server_ip);
		edtServerIp.setText(appPreferences.getString(AppPreferences.SERVER_IP, AppPreferences.DEFAULT_SERVER_IP));
		edtServerIp.setTypeface(ScanActivity.typeThaiSans);
		
		edtServerPort = (EditText) view.findViewById(R.id.server_port);
		edtServerPort.setText(appPreferences.getString(AppPreferences.SERVER_PORT, AppPreferences.DEFAULT_SERVER_PORT));
		edtServerPort.setTypeface(ScanActivity.typeThaiSans);
		
		// Set TypeFace
		((TextView) view.findViewById(R.id.txt_setting)).setTypeface(ScanActivity.typeThaiSans);
		
		((TextView) view.findViewById(R.id.txt_usercode)).setTypeface(ScanActivity.typeThaiSans);
		((TextView) view.findViewById(R.id.txt_password)).setTypeface(ScanActivity.typeThaiSans);
		((TextView) view.findViewById(R.id.txt_device_no)).setTypeface(ScanActivity.typeThaiSans);
//		((TextView) view.findViewById(R.id.txt_store_code)).setTypeface(ScanActivity.typeThaiSans);
		((TextView) view.findViewById(R.id.txt_server_ip)).setTypeface(ScanActivity.typeThaiSans);
		((TextView) view.findViewById(R.id.txt_server_port)).setTypeface(ScanActivity.typeThaiSans);
		
		((TextView) view.findViewById(R.id.txt_cancel)).setTypeface(ScanActivity.typeThaiSans);
		((TextView) view.findViewById(R.id.txt_save)).setTypeface(ScanActivity.typeThaiSans);
		
		txtError = (TextView) view.findViewById(R.id.txt_error);
		txtError.setTypeface(ScanActivity.typeThaiSans);
		
		builder.setView(view);
		AlertDialog alertDialog = builder.create();

		return alertDialog;
	}
	
}
