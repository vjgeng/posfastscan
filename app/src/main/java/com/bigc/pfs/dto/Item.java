package com.bigc.pfs.dto;

import org.json.JSONException;
import org.json.JSONObject;


public class Item {

	private String code;
	private String name;
	private double pricePerUnit;
	private double price;
	private double qnt;
	
	private int id;
	
	public Item() {
		super();
	}
	
	public Item(int dummy) {
		this.code = "1234567890";
		this.name = "BigC สาหร่ายทะเล";
		this.pricePerUnit = 49.00;
		this.price = 98.00;
		this.qnt = 2;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPricePerUnit() {
		return pricePerUnit;
	}
	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getQnt() {
		return qnt;
	}
	public void setQnt(double qnt) {
		this.qnt = qnt;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("prCode", code);
			jsonObj.put("prName", "");
			jsonObj.put("prPrice", "");
			jsonObj.put("qnt", qnt);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jsonObj.toString();
	}
	
}
