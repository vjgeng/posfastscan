package com.bigc.pfs.dto;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {

	private static final ItemRepo instance = new ItemRepo();
	private List<Item> items = new ArrayList<Item>();
	private double sumQnt = 0.00;
	private double sumPrice = 0.00;
	private String pdaRef = "";
	
	private final DecimalFormat formatScanCount = new DecimalFormat("000");
	private int scanCount = 0;

	private ItemRepo() {
		super();
	}

	public static ItemRepo getInstance() {
		return instance;
	}

	public List<Item> getItems() {
		return items;
	}

	public Item getItem(int index) {
		if (index < 0 || index > items.size() - 1) {
			return null;
		} 
		else {
			return items.get(index);
		}
	}

	public void addItem(Item item) {
//		item.setId(scanCount);
		items.add(item);
	}

	public void removeItem(int index) {
		items.remove(index);
	}

	public void clearItem() {
		items.clear();
	}
	
	public void finishSale() {
		clearItem();
		scanCount = 0;
	}

	public int getSize() {
		return items.size();
	}

	public double getSumQnt() {
		return sumQnt;
	}

	public void setSumQnt(double sumQnt) {
		this.sumQnt = sumQnt;
	}

	public double getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice(double sumPrice) {
		this.sumPrice = sumPrice;
	}

	public String getPdaRef() {
		return pdaRef;
	}

	public void setPdaRef(String pdaRef) {
		this.pdaRef = pdaRef;
	}
	
	public String getNextScanCount() {
		scanCount = scanCount + 1;
		return formatScanCount.format(scanCount);
	}

}
