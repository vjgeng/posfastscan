package com.bigc.pfs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.bigc.pfs.activity.R;
import com.bigc.pfs.activity.ScanActivity;

public class DeleteAllDialogFragment extends DialogFragment {

	private ScanActivity scanActivity;
	private int actionKeyCode;
	private boolean isEnter;
	
	public DeleteAllDialogFragment(){}

	public DeleteAllDialogFragment(ScanActivity scanActivity){
		this.scanActivity=scanActivity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
//		String title = "ลบรายการสินค้าทั้งหมด ?";
//		String message = "รายการสินค้าทั้งหมดจะหายไปถาวร \nคุณแน่ใจที่จะลบรายการสินค้าทั้งหมด หรือไม่";
		String title = "ยกเลิกรายการซื้อสินค้า";
		String message = "รายการสินค้าทั้งหมดจะหายไปถาวร";
		
		final Dialog dialog = getDialog();
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		View v = inflater.inflate(R.layout.dialog_confirm, container, false);
		
		((TextView) v.findViewById(R.id.txt_title)).setText(title);
		((TextView) v.findViewById(R.id.txt_message)).setText(message);
		
		final Button btnClose = (Button) v.findViewById(R.id.btn_close);
		btnClose.setTypeface(ScanActivity.typeThaiSans);
		btnClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {

				// Check enter from 'Scanner' or 'Human' -> true:Scanner, false:Human
				if (isEnter) {
					isEnter = false;
					if (actionKeyCode == KeyEvent.KEYCODE_N) {
						dialog.dismiss();
					}
				}
				else {
					dialog.dismiss();	
				}
			}
		});
		
		final Button btnOk = (Button) v.findViewById(R.id.btn_ok);
		btnOk.setTypeface(ScanActivity.typeThaiSans);
		btnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Delete
//				ItemRepo.getInstance().clearItem();
//				scanActivity.notifyDataSetChanged(null, false, false, true);
//				scanActivity.setDefaultMember();
//				scanActivity.setDefaultInput();
//				scanActivity.showToast("ยกเลิกรายการซื้อสินค้าเรียบร้อย");
//				
//				dialog.dismiss();
				
				// Check enter from 'Scanner' or 'Human' -> true:Scanner, false:Human
				if (isEnter) {
					isEnter = false;
					if (actionKeyCode == KeyEvent.KEYCODE_Y) {
						scanActivity.clearItem();
						dialog.dismiss();
					}
				}
				else {
					scanActivity.clearItem();
					dialog.dismiss();
				}
			}
		});
		
		
		// Attach Key Listener
		dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					isEnter = true;
				}
				else if (keyCode == KeyEvent.KEYCODE_Y) {
					actionKeyCode = keyCode;
					btnOk.requestFocusFromTouch();
				}
				else if (keyCode == KeyEvent.KEYCODE_N) {
					actionKeyCode = keyCode;
					btnClose.requestFocusFromTouch();
				}
				
				return false;
			}
		});
		
		return v;
	}
	
}
