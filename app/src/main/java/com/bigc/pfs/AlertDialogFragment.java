package com.bigc.pfs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.bigc.pfs.activity.R;
import com.bigc.pfs.activity.ScanActivity;

public class AlertDialogFragment extends DialogFragment {
	
	private ScanActivity scanActivity;
	private String title;
	private String message;
	private int actionKeyCode;
	private boolean isEnter;
	
	public AlertDialogFragment(ScanActivity scanActivity, String title, String message) {
		this.scanActivity = scanActivity;
		this.title = title;
		this.message = message;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		final Dialog dialog = getDialog();
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		View v = inflater.inflate(R.layout.dialog_alert, container, false);
		
		((TextView) v.findViewById(R.id.txt_title)).setText(title);
		((TextView) v.findViewById(R.id.txt_message)).setText(message);
		
		final Button btnClose = (Button) v.findViewById(R.id.btn_close);
		btnClose.setTypeface(ScanActivity.typeThaiSans);
		btnClose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// Check enter from 'Scanner' or 'Human' -> true:Scanner, false:Human
				if (isEnter) {
					isEnter = false;
					if (actionKeyCode == KeyEvent.KEYCODE_Y) {
						scanActivity.clearTmpScan();
						dialog.dismiss();
					}
				}
				else {
					scanActivity.clearTmpScan();
					dialog.dismiss();
				}
			}
		});
		
		// Attach Key Listener
		dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
			
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					isEnter = true;
				}
				else if (keyCode == KeyEvent.KEYCODE_Y) {
					actionKeyCode = keyCode;
					btnClose.requestFocusFromTouch();
				}
				
				return false;
			}
		});
		
		return v;
	}
	
}
