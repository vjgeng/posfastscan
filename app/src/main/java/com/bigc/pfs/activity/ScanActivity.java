package com.bigc.pfs.activity;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

//import com.bigc.pfs.AlertDialogFragment;
//import com.bigc.pfs.DeleteAllDialogFragment;
//import com.bigc.pfs.DeleteDialogFragment;
import com.bigc.pfs.AlertDialogFragment;
import com.bigc.pfs.DeleteAllDialogFragment;
import com.bigc.pfs.DeleteDialogFragment;
import com.bigc.pfs.ItemListFragmentAdapter;
//import com.bigc.pfs.PdaRefDialogFragment;
//import com.bigc.pfs.SaveDialogFragment;
//import com.bigc.pfs.SettingDialogFragment;
//import com.bigc.pfs.dto.Item;
//import com.bigc.pfs.dto.ItemRepo;
//import com.bigc.pfs.utils.AppPreferences;
//import com.bigc.pfs.utils.Constants;

import com.bigc.pfs.SettingDialogFragment;
import com.bigc.pfs.dto.ItemRepo;
import com.bigc.pfs.utils.AppPreferences;
import com.bigc.pfs.utils.Constants;

//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.entity.UrlEncodedFormEntity;
//import org.apache.http.client.methods.HttpDelete;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.message.BasicHeader;
//import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
//import org.apache.http.protocol.BasicHttpContext;
//import org.apache.http.protocol.HTTP;
//import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

public class ScanActivity extends AppCompatActivity {
	private static final String tag = "ScanActivity";
	
	public static final DecimalFormat formatPrice = new DecimalFormat("#,##0.00");
	public static final DecimalFormat formatQnt = new DecimalFormat("#,##0.###");


	public static final MediaType JSON  = MediaType.get("application/json; charset=utf-8");

	OkHttpClient httpClient = new OkHttpClient();

//	private HttpClient httpClient;
//	private HttpParams httpParameters;
//	private HttpContext localContext = new BasicHttpContext();
	
//	private DummyItemGridFragmentAdapter dummyGridAdapter;
//	private ItemGridFragmentAdapter gridAdapter;

//	private PageIndicator indicator;
//	private ViewPager mPager;
	
	private ItemListFragmentAdapter listAdapter;
	private ListView listViewDetail;
	
	private FragmentManager fm;
	private SharedPreferences appPreferences;
	
//	private String imei;
	
//	private String storeCode;		// 797 -> Find in POSPARA instead
	private String serverIp;		// 172.16.153.82, YYWPOS:10.4.7.162
	private String serverPort;		// 8000
	private String deviceNo;		// 0
	
	public static Typeface typeQuarkBold;
	public static Typeface typeQuarkLight;
	public static Typeface typeThaiSans;
	
	private TextView txtQnt;
	private TextView txtQntWord;
	private TextView txtBarcode;
	private TextView txtBarcodeWord;
	private TextView txtTotal;
	private TextView txtTotalWord;
	private TextView txtReceiptNo;
	
	private TextView txtMember;
	private TextView txtMemberWord;
	
//	private TextView txtListTotalPrice;
//	private TextView txtListTotalQnt;
	
	private View layoutQnt;
	private View layoutBarcode;
	private View layoutMember;
	
//	private View layoutGrid;
	private View layoutList;
	
	private View layoutCalculatorSmall;
	private View layoutCalculatorBig;
	
	private Button btnSave;
	private Button btnRefresh;
	private Button btnCalDone;
	
//	private int viewFlag = 0; 				// 0:grid, 1:list
	private int cartFlag = 0;				// 0:disable, 1:enable
	private int inputFocusFlag = 0;			// 0:qnt, 1:barcode, 2:member
	
	private StringBuilder tmpScan = new StringBuilder();
	
	private double sumQnt = 0;
	private double sumPrice = 0;
	
	public static Animation animSlideTopToBottom;
	private Animation animSlideBottomToTop;
	private Animation animSlideDown;
	private Animation animSlideOut;
	private Animation animSlideIn;
	
//	private ScanProductTask scanProductTask = null;
	
	private MediaPlayer scanSound = null;
	private MediaPlayer errorSound = null;
	
	@Override
    protected void onSaveInstanceState(Bundle saveState) {
		// hack for error when orientation change
		super.onSaveInstanceState(saveState);
    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_scan);
		
		// Get the app's shared preferences
		appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//		storeCode = appPreferences.getString(AppPreferences.STORE_CODE, AppPreferences.DEFAULT_STORE_CODE);
		serverIp = appPreferences.getString(AppPreferences.SERVER_IP, AppPreferences.DEFAULT_SERVER_IP);
		serverPort = appPreferences.getString(AppPreferences.SERVER_PORT, AppPreferences.DEFAULT_SERVER_PORT);
		deviceNo = appPreferences.getString(AppPreferences.DEFAULT_DEVICE_NO, AppPreferences.DEFAULT_DEVICE_NO);
		
		// Due to problem : android.os.StrictMode$AndroidBlockGuardPolicy.onNetwork
		// See : http://stackoverflow.com/questions/6976317/android-http-connection-exception
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		
		// Initial HttpParams
//		initHttpParams();
//		httpClient = new DefaultHttpClient(httpParameters);
		
		// Set IMEI
//		imei = getImei();
		
		// Set View
        txtQnt = (TextView) findViewById(R.id.txt_amount);
        txtQntWord = (TextView) findViewById(R.id.txt_amount_word);
        txtBarcode = (TextView) findViewById(R.id.txt_barcode);
        txtBarcodeWord = (TextView) findViewById(R.id.txt_barcode_word);
        txtTotal = (TextView) findViewById(R.id.txt_total);
        txtTotalWord = (TextView) findViewById(R.id.txt_total_word);
        
        txtMember = (TextView) findViewById(R.id.txt_member);
        txtMemberWord = (TextView) findViewById(R.id.txt_member_word);
        
        layoutQnt = findViewById(R.id.layout_amount);
        layoutBarcode = findViewById(R.id.layout_barcode);
        layoutMember = findViewById(R.id.layout_member);
        
//        layoutGrid = findViewById(R.id.layout_grid);
        layoutList = findViewById(R.id.layout_list);
        
        layoutCalculatorSmall = findViewById(R.id.layout_small_calculator);
    	layoutCalculatorBig = findViewById(R.id.layout_big_calculator);
        
        btnCalDone = ((Button) findViewById(R.id.btn_cal_done));
        btnRefresh = ((Button) findViewById(R.id.btn_refresh));
        btnSave = ((Button) findViewById(R.id.btn_save));
//        btnSave.setOnLongClickListener(new View.OnLongClickListener() {
//			
//			@Override
//			public boolean onLongClick(View v) {
//				if (cartFlag == 1) {
//					confirmDeleteAllDialog();
//				}
//				return false;
//			}
//		});
        
        // Set Typeface
        typeQuarkBold = Typeface.createFromAsset(getAssets(), "Quark_Bold.otf");
        typeQuarkLight = Typeface.createFromAsset(getAssets(), "Quark_Light.otf");
        typeThaiSans = Typeface.createFromAsset(getAssets(), "thaisanslite.otf");
        
        // Set UI Typeface
        txtQnt.setTypeface(typeQuarkBold);
        txtQntWord.setTypeface(typeThaiSans);
        txtBarcode.setTypeface(typeQuarkBold);
        txtBarcodeWord.setTypeface(typeThaiSans);
        
        txtMember.setTypeface(typeQuarkBold);
        txtMemberWord.setTypeface(typeThaiSans);
        
        txtReceiptNo = (TextView) findViewById(R.id.txt_receipt_no);
        
        txtTotal.setTypeface(typeQuarkBold);
        txtTotalWord.setTypeface(typeThaiSans);
        
        ((TextView) findViewById(R.id.txt_receipt_no)).setTypeface(typeQuarkBold);
        btnRefresh.setTypeface(typeThaiSans);
        
        // Load Sound
        scanSound = MediaPlayer.create(this, R.raw.sounds_closed);
        errorSound = MediaPlayer.create(this, R.raw.sounds_error);
        
        // for layout_list
//        ((TextView) findViewById(R.id.txt_header)).setTypeface(typeQuarkBold);
//        txtListTotalPrice = (TextView) findViewById(R.id.txt_total_price);
//        txtListTotalQnt = (TextView) findViewById(R.id.txt_total_qnt);
//        txtListTotalPrice.setTypeface(typeThaiSans);
//        txtListTotalQnt.setTypeface(typeThaiSans);

		/*
		// Initial PdaRef
		if (TextUtils.isEmpty(ItemRepo.getInstance().getPdaRef())) {
			initPdaRef(null);
		}
		else {
			btnRefresh.setVisibility(View.INVISIBLE);
			txtReceiptNo.setText(ItemRepo.getInstance().getPdaRef());
		}
		*/
		
		// Set ViewPager and Indicator
		fm = getSupportFragmentManager();
//		dummyGridAdapter = new DummyItemGridFragmentAdapter(fm);
//		gridAdapter = new ItemGridFragmentAdapter(fm);
//		
//		mPager = (ViewPager) findViewById(R.id.pager);
//		mPager.setAdapter(gridAdapter);
//		indicator = (CirclePageIndicator) findViewById(R.id.indicator);
//		indicator.setViewPager(mPager);


		listAdapter = new ItemListFragmentAdapter(getApplicationContext(), this);
		listViewDetail = (ListView) findViewById(R.id.list_view_detail);
		
//		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//			changeToLayoutList();
//		}
//		else {
//			changeToLayoutGrid();
//		}
//		changeToLayoutList();
		
		// Set Summary
		sumQnt = ItemRepo.getInstance().getSumQnt();
		sumPrice = ItemRepo.getInstance().getSumPrice();
//		setSummary();
		
		// Load Animation

		animSlideTopToBottom = AnimationUtils.loadAnimation(this, R.anim.slide_top_to_bottom);
		animSlideBottomToTop = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_to_top);
		animSlideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
		animSlideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out_cal);
		animSlideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_cal);


		
//		(findViewById(R.id.layout_scan)).setOnKeyListener(this);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_scan, menu);
//		
//		return true;
//	}
//	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//	    
//		// Handle item selection
//	    switch (item.getItemId()) {
//	        case R.id.menu_list:
//	            Log.d(tag, "menu_list click");
//	            changeToLayoutList();
//	            return true;
//	        case R.id.menu_grid:
//	            Log.d(tag, "menu_grid click");
//	            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//	    			showToast("แนวตั้งไม่สามารถแสดงผลแบบกริดได้ กรุณาเปลี่ยนเป็นแนวนอน");
//	    		}
//	            else {
//	            	changeToLayoutGrid();
//	            }
//	            return true;
//	        case R.id.menu_settings:
//	            Log.d(tag, "menu_settings click");
//	            SettingDialogFragment.getInstance(appPreferences).show(fm, "SettingDialog");
//	            return true;
//	        default:
//	            return super.onOptionsItemSelected(item);
//	    }
//	}
//	
//	private void changeToLayoutGrid() {
//		layoutList.setVisibility(View.INVISIBLE);
//        listViewDetail.setAdapter(null);
//        
//        layoutGrid.setVisibility(View.VISIBLE);
//        mPager.setAdapter(gridAdapter);
//        indicator.setCurrentItem(0);
//        
//        viewFlag = 0;
//	}
	
//	private void changeToLayoutList() {
//		layoutList.setVisibility(View.VISIBLE);
//        listViewDetail.setAdapter(listAdapter);
//	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if (event.getFlags() == 0) {		// check for fix android 4.2 fire onKeyDown twice
			tmpScan.append((char) event.getUnicodeChar());
			
			btnCalDone.requestFocusFromTouch();	
		}
		
		return false;
	}
	
	public void clearTmpScan() {
		tmpScan = new StringBuilder();
	}
	
	public ScanActivity getScanActivity() {
		return this;
	}

	/*
	public boolean validateScan(String qnt, String barcode, String pdaRef) {
		boolean isOk = true;
		
		if (TextUtils.isEmpty(pdaRef)) {
			
			errorSound.start();
			(new AlertDialogFragment(this, Constants.RESERVE_REQUIRE_TITLE, Constants.RESERVE_REQUIRE_MSG)).show(fm, "ReserveRequire");
			return false;
		}
		
//		if (TextUtils.isEmpty(qnt)) {
//			isOk = false;
//			txtQntWord.setTextColor(getResources().getColor(R.color.red));
//		}
		if (".".equals(qnt)) {
			isOk = false;
			txtQntWord.setTextColor(getResources().getColor(R.color.red));
		}
		else if (Double.parseDouble(qnt) <= 0) {
			isOk = false;
			txtQntWord.setTextColor(getResources().getColor(R.color.red));
		}
		else {
			txtQntWord.setTextColor(getResources().getColor(R.color.darkgrey));
		}
		
		if (TextUtils.isEmpty(barcode)) {
			isOk = false;
			txtBarcodeWord.setTextColor(getResources().getColor(R.color.red));
			if (inputFocusFlag == 1) {
				txtBarcodeWord.setVisibility(View.VISIBLE);
			}
		}
		else {
			txtBarcodeWord.setTextColor(getResources().getColor(R.color.darkgrey));
		}
		
		return isOk;
	}
	
	public void scanProduct(String qnt, String barcode, String pdaRef) {
		if (ItemRepo.getInstance().getSize() >= 300) {
			showToast("ไม่สามารถแสกนสินค้าเกิน 300 รายการได้");
		}
		else {
			// comment for multiple request
//			if (scanProductTask != null) {
//				Log.d(tag, "## [reject] scanProduct barcode="+barcode+", qnt="+qnt+"");	
//				return;
//			}
			
			Log.d(tag, "## scanProduct barcode="+barcode+", qnt="+qnt+"");
			
			scanProductTask = new ScanProductTask();
			scanProductTask.execute(Constants.RS_SCAN, qnt, barcode, pdaRef);	
		}
	}
	*/
	public void toggleInputBackground(View v) {

		/*
		switch (v.getId()) {
			case R.id.layout_member:
				layoutBarcode.setBackgroundResource(R.drawable.bg_text_gray);
				layoutBarcode.setPadding(0, 0, 0, 0);
				layoutQnt.setBackgroundResource(R.drawable.bg_text_gray);
				layoutQnt.setPadding(0, 0, 0, 0);
				v.setBackgroundResource(R.drawable.bg_text_gray_focus);
				v.setPadding(0, 0, 0, 0);
				txtMemberWord.setVisibility(View.INVISIBLE);
				if (txtBarcode.getText().length() > 0) {
					txtBarcodeWord.setVisibility(View.INVISIBLE);
				}
				else {
					txtBarcodeWord.setVisibility(View.VISIBLE);
				}
				inputFocusFlag = 2;
				
				showBigCalculator();
				
				break;
			
			case R.id.layout_amount:
				layoutBarcode.setBackgroundResource(R.drawable.bg_text_gray);
				layoutBarcode.setPadding(0, 0, 0, 0);
				layoutMember.setBackgroundResource(R.drawable.bg_text_gray);
				layoutMember.setPadding(0, 0, 0, 0);
				v.setBackgroundResource(R.drawable.bg_text_gray_focus);
				v.setPadding(0, 0, 0, 0);
				if (txtBarcode.getText().length() > 0) {
					txtBarcodeWord.setVisibility(View.INVISIBLE);
				}
				else {
					txtBarcodeWord.setVisibility(View.VISIBLE);
				}
				if (txtMember.getText().length() > 0) {
					txtMemberWord.setVisibility(View.INVISIBLE);
				}
				else {
					txtMemberWord.setVisibility(View.VISIBLE);
				}
				inputFocusFlag = 0;
				
				hideBigCalculator();
				
				break;
	
			case R.id.layout_barcode:
				layoutQnt.setBackgroundResource(R.drawable.bg_text_gray);
				layoutQnt.setPadding(0, 0, 0, 0);
				layoutMember.setBackgroundResource(R.drawable.bg_text_gray);
				layoutMember.setPadding(0, 0, 0, 0);
				v.setBackgroundResource(R.drawable.bg_text_gray_focus);
				v.setPadding(0, 0, 0, 0);
				txtBarcodeWord.setVisibility(View.INVISIBLE);
				if (txtMember.getText().length() > 0) {
					txtMemberWord.setVisibility(View.INVISIBLE);
				}
				else {
					txtMemberWord.setVisibility(View.VISIBLE);
				}
				inputFocusFlag = 1;
				
				showBigCalculator();
				
				break;
		}
		*/
	}

	/*
	private void showBigCalculator() {
		if (layoutCalculatorBig.getVisibility() == View.INVISIBLE) {
			layoutCalculatorSmall.startAnimation(animSlideOut);
			layoutCalculatorSmall.setVisibility(View.INVISIBLE);
			layoutCalculatorBig.startAnimation(animSlideBottomToTop);
			layoutCalculatorBig.setVisibility(View.VISIBLE);
		}
	}
	
	private void hideBigCalculator() {
		if (layoutCalculatorBig.getVisibility() == View.VISIBLE) {
			layoutCalculatorBig.startAnimation(animSlideDown);
			layoutCalculatorBig.setVisibility(View.INVISIBLE);
			layoutCalculatorSmall.startAnimation(animSlideIn);
			layoutCalculatorSmall.setVisibility(View.VISIBLE);
		}
	}
	
	public void toggleCartSaveBackground() {
		if (cartFlag == 0 && ItemRepo.getInstance().getSize() > 0) {
			btnSave.setBackgroundResource(R.drawable.cart);
			cartFlag = 1;
		}
		else if (cartFlag == 1 && ItemRepo.getInstance().getSize() == 0) {
			btnSave.setBackgroundResource(R.drawable.cart_blank);
			cartFlag = 0;
		}
	}

	*/
	public void calButtonClick(View v) {
		
		TextView focusView;
		if (inputFocusFlag == 1) {
			focusView = txtBarcode;
			txtBarcodeWord.setVisibility(View.INVISIBLE);
		}
		else if (inputFocusFlag == 2) {
			focusView = txtMember;
			txtMemberWord.setVisibility(View.INVISIBLE);
		}
		else {
			focusView = txtQnt;
		}
		
		String old = focusView.getText().toString();
		
		switch (v.getId()) {
			case R.id.btn_cal_back:
				if (old.length() > 0) {
					focusView.setText(old.substring(0, old.length() - 1));	
				}
				break;
			
			case R.id.btn_cal_dot:
				if (inputFocusFlag == 0 && old.indexOf(".") == -1) {
					focusView.setText(old + ".");
				}
				break;				
	
			default:
				int dotIndex = old.indexOf(".");
				if (dotIndex > -1 && old.length() - dotIndex > 3) {
					
				}
				else if(inputFocusFlag == 0 && old.length() == 3 && dotIndex == -1) {
					
				}
				else {
					String input = ((Button) v).getText().toString();
					focusView.setText(old + input);	
				}
				break;
		}
		
//		btnCalDone.requestFocusFromTouch();
	}

	public void calDoneClick(View v) {

		String scan = tmpScan.toString();
		
		if (Constants.SHORTCUT_MEMBER.equals(scan)) {
			toggleInputBackground(layoutMember);
		}
		else if (Constants.SHORTCUT_FINISH.equals(scan)) {
			confirmSaveDialog(btnSave);
		}
//		else if (scan.indexOf(Constants.SHORTCUT_QNT) == 0) {		// *[N] such as *1, *2, *3 ...
//			txtQnt.setText(scan.substring(1, scan.length()));
//		}
		else if (tmpScan.length() > 3 && Constants.SHORTCUT_QNT.charAt(0) == tmpScan.charAt(2)) {		// *[N] such as *1, *2, *3 ...
			txtQnt.setText(scan.substring(3, scan.length()));
		}
		else if (inputFocusFlag == 0 || inputFocusFlag == 1) {
			String pdaRef = txtReceiptNo.getText().toString();
			String qnt = txtQnt.getText().toString();
			String barcode = tmpScan.toString();
			
			if (TextUtils.isEmpty(qnt)) {
				qnt = "1";
			}
			if (TextUtils.isEmpty(barcode)) {
				barcode = txtBarcode.getText().toString();
			}

	/*		if (validateScan(qnt, barcode, pdaRef)) {
				
				scanProduct(qnt, barcode, pdaRef);
//				scanProduct(qnt, barcode, pdaRef);	// TODO: remove
//				scanProduct(qnt, barcode, pdaRef);	// TODO: remove
//				scanProduct(qnt, barcode, pdaRef);	// TODO: remove
//				scanProduct(qnt, barcode, pdaRef);	// TODO: remove
				
				setDefaultInput();
			}*/
		}
		else if (inputFocusFlag == 2) {
			
			if (! TextUtils.isEmpty(scan)) {
				txtMember.setText(scan);
			}
			
			boolean collectMember = true;
			
			String member = txtMember.getText().toString();
			
			if (member.length() == 0) {
				
			}
			else if (member.length() > 18) {
				collectMember = false;			// exceed maximum 18 char
			}
			else if ("0".equals(member.substring(0, 1)) && member.length() > 10) {
				collectMember = false;			// begin with "0" is telephone number, not exceed 10 char
			}
			else {
				String regex = "[0-9]+";
				if (! member.matches(regex)) {
					collectMember = false;		// must contain only number
				}
			}
			
			if (collectMember) {
				toggleInputBackground(layoutQnt);	
			}
			else {
				txtMember.setText("");
				errorSound.start();
				(new AlertDialogFragment(this, Constants.MEMBER_INCORRECT_TITLE+" "+member+" !!!", Constants.MEMBER_INCORRECT_MSG)).show(fm, "MemberIncorrect");
			}
		}
		
		tmpScan = new StringBuilder();
	}

	/*
	
	public void setDefaultInput() {
		
		txtQnt.setText("");
		txtBarcode.setText("");
		
		txtQntWord.setTextColor(getResources().getColor(R.color.darkgrey));
		txtBarcodeWord.setTextColor(getResources().getColor(R.color.darkgrey));
		
		toggleInputBackground(layoutQnt);
	}
	
	public void setDefaultMember() {
		txtMember.setText("");
	}
	
	public void notifyDataSetChanged(Item item, boolean isAdd, boolean isDelete, boolean isClear) {
		
		// Set Summary
		if (isAdd) {
			sumQnt = sumQnt + item.getQnt();
			sumPrice = sumPrice + item.getPrice();	
		}
		else if (isDelete) {
			sumQnt = sumQnt - item.getQnt();
			sumPrice = sumPrice - item.getPrice();
		}
		else if (isClear) {
			sumQnt = 0.00;
			sumPrice = 0.00;
		}
		ItemRepo.getInstance().setSumQnt(sumQnt);
		ItemRepo.getInstance().setSumPrice(sumPrice);
		
		// Set Summary
		setSummary();
		
		// Refresh ViewPager
//		if (viewFlag == 0) {		// layout_grid
//			gridAdapter.notifyDataSetChanged();
//			indicator.notifyDataSetChanged();	
//		}
//		else {
//			listAdapter.notifyDataSetChanged();
//		}
		
		listAdapter.notifyDataSetChanged();
		
//		listViewDetail.smoothScrollToPosition(0);
		listViewDetail.setSelection(0);
	}
	
	public void setSummary() {
		String strSumPrice = formatPrice.format(ItemRepo.getInstance().getSumPrice());
		String strSumQnt = formatQnt.format(ItemRepo.getInstance().getSumQnt());
		
		txtTotal.setText(strSumPrice + " ฿");
		txtTotalWord.setText("รวมยอดเงิน ("+strSumQnt+" ชิ้น) :");
		
		// for layout_list
//		txtListTotalPrice.setText("ราคารวม " + strSumPrice + " ฿");
//		txtListTotalQnt.setText("จำนวนขาย " + strSumQnt + " ชิ้น");
		
		// Set Cart Flag
		toggleCartSaveBackground();
	}
	
	public void showToast(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}

	*/

	public void btnSettingClick(View view) {
		SettingDialogFragment.getInstance(appPreferences).show(fm, "SettingDialog");
	}
	
	public void btnRecycleBinClick(View view) {
		/*
		if (cartFlag == 1) {
			confirmDeleteAllDialog();
		}
		*/
	}
	
	public void cancelSetting(View view) {
		SettingDialogFragment.getInstance(appPreferences).dismiss();
	}
	
	public void saveSetting(View view) {

		SettingDialogFragment settingDialog = SettingDialogFragment.getInstance(appPreferences);
		settingDialog.getTxtError().setVisibility(View.GONE);
		
		String userId = settingDialog.getEdtUsercode().getText().toString();
		String password = settingDialog.getEdtPassword().getText().toString();
		String deviceNo = settingDialog.getEdtDeviceNo().getText().toString();
		String serverIp = settingDialog.getEdtServerIp().getText().toString();
		String serverPort = settingDialog.getEdtServerPort().getText().toString();
		
		//validateUserCanSetting(userId, password, deviceNo, serverIp, serverPort);

	}

	/*
	public void canSettingResult(String result) {
		
		SettingDialogFragment settingDialog = SettingDialogFragment.getInstance(appPreferences);
		
		if ("Y".equals(result)) {
			
//			String storeCode = settingDialog.getEdtStoreCode().getText().toString();
			String serverIp = settingDialog.getEdtServerIp().getText().toString();
			String serverPort = settingDialog.getEdtServerPort().getText().toString();
			String deviceNo = settingDialog.getEdtDeviceNo().getText().toString();
			
			SharedPreferences.Editor editor = appPreferences.edit();
//			if (! TextUtils.isEmpty(storeCode)) {
//				editor.putString(AppPreferences.STORE_CODE, storeCode);
//				this.storeCode = storeCode;
//			}
			if (! TextUtils.isEmpty(serverIp)) {
				editor.putString(AppPreferences.SERVER_IP, serverIp);
				this.serverIp = serverIp;
			}
			if (! TextUtils.isEmpty(serverPort)) {
				editor.putString(AppPreferences.SERVER_PORT, serverPort);
				this.serverPort = serverPort;
			}
			if (! TextUtils.isEmpty(deviceNo)) {
				editor.putString(AppPreferences.DEVICE_NO, deviceNo);
				this.deviceNo = deviceNo;
			}
			editor.commit();
			
			settingDialog.dismiss();
			
			showToast("บันทึกการตั้งค่าโปรแกรมใหม่เรียบร้อย");
		}
		else {
			settingDialog.getTxtError().setText(result);
			settingDialog.getTxtError().setVisibility(View.VISIBLE);
		}
	}
	
	private void initHttpParams() {
		httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used.
		HttpConnectionParams.setConnectionTimeout(httpParameters, Constants.REST_TIMEOUT_CONNECTION);
		// Set the default socket timeout (SO_TIMEOUT) 
		// in milliseconds which is the timeout for waiting for data.
		HttpConnectionParams.setSoTimeout(httpParameters, Constants.REST_TIMEOUT_SOCKET);
	}
	
//	private String getImei() {
//		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//		return telephonyManager.getDeviceId();
//	}
	*/

	public void initPdaRef(View v) {
		/*
		if (scanProductTask != null) {
			return;
		}
		
		scanProductTask = new ScanProductTask();
		scanProductTask.execute(Constants.RS_RESERVE);
		*/
	}

	public void confirmDeleteDialog(int repoIndex) {
		(new DeleteDialogFragment(this, repoIndex)).show(fm, "DeleteItem");
	}
	
	public void confirmDeleteAllDialog() {
		(new DeleteAllDialogFragment(this)).show(fm, "DeleteAllItem");

	}

	public void confirmSaveDialog(View v) {

		/*
		if (cartFlag == 1) {
			String receiptNo = txtReceiptNo.getText().toString();
			
			String amountWord = txtTotalWord.getText().toString();
			amountWord = amountWord.substring(12, amountWord.indexOf(")"));
			String summary = "รวมจำนวนสินค้า "+amountWord+"\n"+"จำนวนเงิน "+txtTotal.getText().toString();
			
//			String summary = txtTotalWord.getText().toString() + " " + txtTotal.getText().toString();
			
			(new SaveDialogFragment(receiptNo, summary)).show(fm, "SaveItem");	
		}
		*/
	}

	/*
	public void saveItem() {
		if (scanProductTask != null) {
			return;
		}
		
		scanProductTask = new ScanProductTask();
		scanProductTask.execute(Constants.RS_SAVE, txtMember.getText().toString());
	}
	
	public void validateUserCanSetting(String userId, String password, String deviceNo, String serverIp, String serverPort) {
		
		if (TextUtils.isEmpty(userId)
				|| TextUtils.isEmpty(password)
				|| TextUtils.isEmpty(deviceNo)
				|| TextUtils.isEmpty(serverIp)
				|| TextUtils.isEmpty(serverPort)) {
			canSettingResult("* กรุณากรอกข้อมูลให้ครบถ้วน");
		}
		else {
			if (scanProductTask != null) {
				return;
			}
			
			scanProductTask = new ScanProductTask();
			scanProductTask.execute(Constants.RS_SETTING, userId, password, serverIp, serverPort);
		}
	}*/
	
	public void deleteItem(int repoIndex) {
		
		/* temp if (scanProductTask != null) {
			return;
		}
		
		scanProductTask = new ScanProductTask();
		scanProductTask.execute(Constants.RS_DELETE, txtReceiptNo.getText().toString(), repoIndex+"");*/
	}
	
	public void clearItem() {
		
	/* temp	if (scanProductTask != null) {
			return;
		}
		
		scanProductTask = new ScanProductTask();
		scanProductTask.execute(Constants.RS_CLEAR, txtReceiptNo.getText().toString());*/
	}
/*
	private class ScanProductTask extends AsyncTask<String, Void, Map<String, Object>> {
		
		private String method;
		private String qnt;
		private String barcode;
		private String pdaRef;
		private String member;
		
		private int deleteRepoIndex;
		private Item deleteItem;
		
		protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
			InputStream in = entity.getContent();
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n > 0) {
				byte[] b = new byte[4096];
				n = in.read(b);
				if (n > 0)
					out.append(new String(b, 0, n));
			}
			return out.toString();
		}

		@Override
		protected void onPreExecute() {
			// TODO: Show progress bar
			// ...
		}

		@Override
		protected Map<String, Object> doInBackground(String... params) {
			
			Map<String, Object> taskResp = new HashMap<String, Object>();
			HttpResponse httpResponse = null;
//			HttpClient httpClient = new DefaultHttpClient(httpParameters);
//			HttpContext localContext = new BasicHttpContext();
			
			try {
				method = params[0];
				Log.d(tag, "## ScanProductTask.doInBackground method="+method+"");
				
				// Scan
				if (Constants.RS_SCAN.equals(method)) {
					
					qnt = params[1];
					barcode = params[2];
					pdaRef = params[3];
					
					// barcode only have number
					if (barcode.matches("[0-9]+")) {
						if (barcode.length() < 13) {
							barcode = "0000000000000" + barcode;
							barcode = barcode.substring(barcode.length()-13);
						}	
					}
					else {
						errorSound.start();
						(new AlertDialogFragment(getScanActivity(), "ไม่พบข้อมูลรหัสสินค้า " + barcode + " !!!", Constants.SCAN_NOTFOUND_MSG)).show(fm, "ScanNotFound");
						return taskResp;
					}
					
					String rowId = ItemRepo.getInstance().getNextScanCount();
					taskResp.put("rowId", rowId);
					
					StringBuilder uri = new StringBuilder();
					uri.append("http://"+serverIp+":"+serverPort+"/PFSWEB/rest/fastScan");
					uri.append("?barcode="+barcode);
					uri.append("&chkStck=P"+rowId);
					uri.append("&qnt="+qnt);
					uri.append("&ref="+pdaRef);
					
					HttpGet httpGet = new HttpGet(uri.toString());
					httpGet.setHeader("Cache-Control", "no-store");
					
					try {
						httpResponse = httpClient.execute(httpGet, localContext);
					}
					catch (IOException e) {
						errorSound.start();
//						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, Constants.CONNECTION_ERROR_MSG)).show(fm, "ScanError");
						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, e.toString())).show(fm, "ScanError");
						
						Log.w(tag, "IOException in ScanProductTask Scan cause : " + e.getMessage());
						e.printStackTrace();
					}
				}
				// Save
				else if (Constants.RS_SAVE.equals(method)) {
					
					member = params[1];
					
					// Generate JSON Object
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("imei", deviceNo);
					jsonObj.put("pdaRef", txtReceiptNo.getText().toString());
					
					JSONArray jsonArr = new JSONArray();
					
					if (! TextUtils.isEmpty(member)) {
						JSONObject obj = new JSONObject();
						obj.put("barcode", member);
						obj.put("prName", "");
						obj.put("prPrice", "");
						obj.put("qnt", 1);
						obj.put("row", -1);
						
						jsonArr.put(obj);
					}
					
					List<Item> items = ItemRepo.getInstance().getItems();
					int i = 1;
					for (Item item : items) {
						JSONObject obj = new JSONObject();
						obj.put("barcode", item.getCode());
						obj.put("prName", "");
						obj.put("prPrice", "");
						obj.put("qnt", item.getQnt());
						obj.put("row", i);
						i++;
						
						jsonArr.put(obj);
					}
					
					jsonObj.put("items", jsonArr);
					
					Log.d(tag, " |- Save json : " + jsonArr.length() + " items");
					
					// Post to WebService
					HttpPost httpPost = new HttpPost("http://"+serverIp+":"+serverPort+"/PFSWEB/rest/fastScan");
					StringEntity entity = new StringEntity(jsonObj.toString());  
					entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
	                httpPost.setEntity(entity);
	                
					try {
	                    httpResponse = httpClient.execute(httpPost, localContext);
					}
					catch (IOException e) {
						errorSound.start();
//						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, Constants.CONNECTION_ERROR_MSG)).show(fm, "SaveError");
						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, e.toString())).show(fm, "SaveError");
						
						Log.w(tag, "IOException in ScanProductTask Save cause : " + e.getMessage());
						e.printStackTrace();
					}
				}
				// Reserve
				else if (Constants.RS_RESERVE.equals(method)) {
					
					HttpGet httpGet = new HttpGet("http://"+serverIp+":"+serverPort+"/PFSWEB/rest/fastScan/reserve/" + deviceNo);
					httpGet.setHeader("Cache-Control", "no-store");
					
					try {
						httpResponse = httpClient.execute(httpGet, localContext);
					} 
					catch (IOException e) {
						errorSound.start();
//						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, Constants.CONNECTION_ERROR_MSG)).show(fm, "ReserveError");
						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, e.toString())).show(fm, "ReserveError");
						btnRefresh.setVisibility(View.VISIBLE);


						Log.w(tag, "IOException in ScanProductTask Reserve cause : " + e.getMessage());
						e.printStackTrace();
					}
				}
				// Delete
				else if (Constants.RS_DELETE.equals(method)) {
					
					deleteRepoIndex = Integer.parseInt(params[2]);
					deleteItem = ItemRepo.getInstance().getItem(deleteRepoIndex);
					
					HttpDelete httpDelete = new HttpDelete("http://"+serverIp+":"+serverPort+"/PFSWEB/rest/fastScan/deleteItem/"+params[1]+"/"+deleteItem.getId());
					
					try {
	                    httpResponse = httpClient.execute(httpDelete, localContext);
					} 
					catch (IOException e) {
						errorSound.start();
//						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, Constants.CONNECTION_ERROR_MSG)).show(fm, "DeleteError");
						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, e.toString())).show(fm, "DeleteError");
						
						Log.w(tag, "IOException in ScanProductTask Delete cause : " + e.getMessage());
						e.printStackTrace();
					}
				}
				// Delete
				else if (Constants.RS_CLEAR.equals(method)) {
					
					HttpDelete httpDelete = new HttpDelete("http://"+serverIp+":"+serverPort+"/PFSWEB/rest/fastScan/clearItem/"+params[1]);
					
					try {
	                    httpResponse = httpClient.execute(httpDelete, localContext);
					} 
					catch (IOException e) {
						errorSound.start();
//						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, Constants.CONNECTION_ERROR_MSG)).show(fm, "ClearError");
						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, e.toString())).show(fm, "ClearError");
						
						Log.w(tag, "IOException in ScanProductTask Clear cause : " + e.getMessage());
						e.printStackTrace();
					}
				}
				// Setting
				else if (Constants.RS_SETTING.equals(method)) {
					
					HttpPost httpPost = new HttpPost("http://"+params[3]+":"+params[4]+"/PFSWEB/rest/fastScan/checkUserCanSetting");
					
					List<NameValuePair> form = new ArrayList<NameValuePair>();
					form.add(new BasicNameValuePair("userId", params[1]));
					form.add(new BasicNameValuePair("password", params[2]));
					
					httpPost.setEntity(new UrlEncodedFormEntity(form));
					
					try {
	                    httpResponse = httpClient.execute(httpPost, localContext);
					} 
					catch (IOException e) {
						errorSound.start();
//						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, Constants.CONNECTION_ERROR_MSG)).show(fm, "SettingError");
						(new AlertDialogFragment(getScanActivity(), Constants.CONNECTION_ERROR_TITLE, e.toString())).show(fm, "SettingError");
						
						Log.w(tag, "IOException in ScanProductTask Setting cause : " + e.getMessage());
						e.printStackTrace();
					}
				}
				
				taskResp.put("httpResponse", httpResponse);
				
			} catch (Exception e) {
				errorSound.start();
				(new AlertDialogFragment(getScanActivity(), "เกิดข้อผิดพลาด  !!!", e.toString())).show(fm, "DoInBackgroundError");
				
				Log.w(tag, "Exception in ScanProductTask.doInBackground cause : " + e.getMessage());
				e.printStackTrace();
			}
			
			return taskResp;
		}

		@Override
		protected void onPostExecute(Map<String, Object> taskResp) {
			
			Log.d(tag, "## ScanProductTask.onPostExecute method="+method+"");
			HttpResponse resp = (HttpResponse) taskResp.get("httpResponse");
			
			try {
				if (resp != null) {
					
					int respCode = resp.getStatusLine().getStatusCode();
					Log.d(tag, " |- respCode="+respCode+"");
					
					// Scan
					if (Constants.RS_SCAN.equals(method)) {
						
						if (respCode == 200) {
							
							String rowId = (String) taskResp.get("rowId");
							
							String respString = getASCIIContentFromEntity(resp.getEntity());
							
							JSONObject jsonObj = new JSONObject(respString);
							
							// Check sale limit
							String nosaleStt = jsonObj.getString("nosaleStt");
							if ("1".equals(nosaleStt)) {
								String nosaleMsg = jsonObj.getString("nosaleMsg");
								
								errorSound.start();
								(new AlertDialogFragment(getScanActivity(), Constants.NOSALE_TITLE, nosaleMsg)).show(fm, "NoSaleFound");
							}
							else {

//								String prCode = jsonObj.getString("prCode");
								String prCode = jsonObj.getString("barcode");
								String prName = jsonObj.getString("prName");
								prName = prName.trim();
								
								double doubleQnt;
								if (TextUtils.isEmpty(qnt)) {
									doubleQnt = 1;
								}
								else {
									doubleQnt = Double.parseDouble(qnt);
								}
								
								String prType = jsonObj.getString("prType");
								double weight = jsonObj.getDouble("weight");
								double prPrice = jsonObj.getDouble("prPrice");
								
								double totalPrice;
								if (Constants.PR_TYPE_FRESHFOOD.equals(prType)) {
									doubleQnt = weight;
									totalPrice = jsonObj.getDouble("amt");
									prPrice = 0;
								}
								else {
									totalPrice = doubleQnt * prPrice;
								}
								
								Item item = new Item();
								item.setId(Integer.parseInt(rowId));
								item.setCode(prCode);
								item.setName(prName);
								item.setPricePerUnit(prPrice);
								item.setPrice(totalPrice);
								item.setQnt(doubleQnt);
								
								ItemRepo.getInstance().addItem(item);
								notifyDataSetChanged(item, true, false, false);
								
								// Play sound
								scanSound.start();
							}
						}
						else {
							// Not found this barcode
//							(new AlertDialogFragment(Constants.SCAN_NOTFOUND_TITLE+" "+barcode+" !!!", Constants.SCAN_NOTFOUND_MSG)).show(fm, "ScanNotFound");
							String respString = getASCIIContentFromEntity(resp.getEntity());
							errorSound.start();
							(new AlertDialogFragment(getScanActivity(), respString, Constants.SCAN_NOTFOUND_MSG)).show(fm, "ScanNotFound");
						}
					}
					// Save
					else if (Constants.RS_SAVE.equals(method)) {
						
						if (respCode == 200) {
							
							String respString = getASCIIContentFromEntity(resp.getEntity());
							ItemRepo.getInstance().setPdaRef(respString);
							
							String oldPdaRef = txtReceiptNo.getText().toString();
							
							txtReceiptNo.setText(respString);
							ItemRepo.getInstance().finishSale();
							notifyDataSetChanged(null, false, false, true);
							
							setDefaultMember();
							setDefaultInput();
							
							// TODO: show dialog resp
							// ...
							(new PdaRefDialogFragment(oldPdaRef)).show(fm, "SaveSuccess");
							
						}
						else {
							errorSound.start();
							(new AlertDialogFragment(getScanActivity(), Constants.SERVERERROR_TITLE+" (ResponseCode:"+respCode+")", Constants.SERVERERROR_MSG)).show(fm, "SaveServerError");
						}
					}
					// Reserve
					else if (Constants.RS_RESERVE.equals(method)) {
						
						if (respCode == 200) {
							
							String respString = getASCIIContentFromEntity(resp.getEntity());
							ItemRepo.getInstance().setPdaRef(respString);
							
							btnRefresh.setVisibility(View.INVISIBLE);
							txtReceiptNo.setText(respString);
						}
						else {
							errorSound.start();
							(new AlertDialogFragment(getScanActivity(), Constants.SERVERERROR_TITLE+" (ResponseCode:"+respCode+")", Constants.SERVERERROR_MSG)).show(fm, "ReserveError");
							btnRefresh.setVisibility(View.VISIBLE);
						}
					}
					// Delete
					else if (Constants.RS_DELETE.equals(method)) {
						
						if (respCode == 200) {
							
//							String respString = getASCIIContentFromEntity(resp.getEntity());
//							if ("0".equals(respString)) {
//								(new AlertDialogFragment(getScanActivity(), Constants.SERVERERROR_TITLE, Constants.SERVERERROR_MSG)).show(fm, "DeleteError");
//							}
//							else {
								ItemRepo.getInstance().removeItem(deleteRepoIndex);
								notifyDataSetChanged(deleteItem, false, true, false);
								showToast("ลบรายการสินค้าเรียบร้อย");
//							}
						}
						else {
							errorSound.start();
							(new AlertDialogFragment(getScanActivity(), Constants.SERVERERROR_TITLE+" (ResponseCode:"+respCode+")", Constants.SERVERERROR_MSG)).show(fm, "DeleteError");
						}
					}
					// Clear
					else if (Constants.RS_CLEAR.equals(method)) {
						
						if (respCode == 200) {
							
//							String respString = getASCIIContentFromEntity(resp.getEntity());
//							if ("0".equals(respString)) {
//								(new AlertDialogFragment(getScanActivity(), Constants.SERVERERROR_TITLE, Constants.SERVERERROR_MSG)).show(fm, "ClearError");
//							}
//							else {
								ItemRepo.getInstance().clearItem();
								notifyDataSetChanged(null, false, false, true);
								setDefaultMember();
								setDefaultInput();
								showToast("ยกเลิกรายการซื้อสินค้าเรียบร้อย");
//							}
						}
						else {
							errorSound.start();
							(new AlertDialogFragment(getScanActivity(), Constants.SERVERERROR_TITLE+" (ResponseCode:"+respCode+")", Constants.SERVERERROR_MSG)).show(fm, "ClearError");
						}
					}
					// Setting
					else if (Constants.RS_SETTING.equals(method)) {
						
						if (respCode == 200) {
							
							String respString = getASCIIContentFromEntity(resp.getEntity());
							canSettingResult(respString);
						}
						else {
							errorSound.start();
							(new AlertDialogFragment(getScanActivity(), Constants.SERVERERROR_TITLE+" (ResponseCode:"+respCode+")", Constants.SERVERERROR_MSG)).show(fm, "SettingError");
						}
					}
				}
			} catch (Exception e) {
				errorSound.start();
				(new AlertDialogFragment(getScanActivity(), "เกิดข้อผิดพลาด  !!!", e.toString())).show(fm, "OnPostExecuteError");
				
				Log.e(tag, "Exception in ScanProductTask.onPostExecute cause : " + e.getMessage());
				e.printStackTrace();
			}
			
			scanProductTask = null;
			// TODO: Hide progress bar
			// ...
		}
		
	}

	*/
}
