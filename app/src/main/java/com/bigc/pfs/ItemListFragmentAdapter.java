package com.bigc.pfs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bigc.pfs.activity.R;
import com.bigc.pfs.activity.ScanActivity;
import com.bigc.pfs.dto.Item;
import com.bigc.pfs.dto.ItemRepo;

public class ItemListFragmentAdapter extends BaseAdapter {
	
	private LayoutInflater mInflater;
	private ScanActivity scanActivity;
	
	public ItemListFragmentAdapter(Context context, ScanActivity scanActivity) {
		mInflater = LayoutInflater.from(context);
		this.scanActivity = scanActivity;
	}
	
//	@Override
//	public boolean isEnabled(int position) {
//		return false;
//	}

	@Override
	public int getCount() {
		return ItemRepo.getInstance().getSize();
	}

	@Override
	public Object getItem(int position) {
		return ItemRepo.getInstance().getItem(getCount()-position-1);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
//		TextView txtName;
//		TextView txtPrice;
//		TextView txtQnt;
//		
//		TextView txtLineNo;
//		TextView txtBarcode;
		
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_view_row, null);
			holder = new ViewHolder();
			
			holder.txtName = (TextView) convertView.findViewById(R.id.name);
			holder.txtPrice = (TextView) convertView.findViewById(R.id.price);
			holder.txtQnt = (TextView) convertView.findViewById(R.id.qnt);
			
			holder.txtLineNo = (TextView) convertView.findViewById(R.id.lineNo);
			holder.txtBarcode = (TextView) convertView.findViewById(R.id.barcode);
			holder.txtPricePerUnit = (TextView) convertView.findViewById(R.id.pricePerUnit);
			
			holder.txtName.setTypeface(ScanActivity.typeThaiSans);
			holder.txtPrice.setTypeface(ScanActivity.typeThaiSans);
			holder.txtQnt.setTypeface(ScanActivity.typeThaiSans);
			
			holder.txtLineNo.setTypeface(ScanActivity.typeThaiSans);
			holder.txtBarcode.setTypeface(ScanActivity.typeThaiSans);
			holder.txtPricePerUnit.setTypeface(ScanActivity.typeThaiSans);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Item item = (Item) getItem(position);
		
		String strQnt = ScanActivity.formatQnt.format(item.getQnt());
		
		holder.txtName.setText(item.getName());
		holder.txtPrice.setText(ScanActivity.formatPrice.format(item.getPrice()));
		holder.txtQnt.setText(strQnt);
		
//		if (strQnt.length() > 3) {
//			holder.txtQnt.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));	
//		}
		
		holder.txtLineNo.setText(Integer.toString(getCount()-position));
		holder.txtBarcode.setText(item.getCode());
		
		if (item.getPricePerUnit() > 0) {
			holder.txtPricePerUnit.setText("@"+ScanActivity.formatPrice.format(item.getPricePerUnit())+"/หน่วย");
		}
		
		(convertView.findViewById(R.id.btnDelete)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				scanActivity.confirmDeleteDialog(getCount()-position-1);
			}
		});
		
//		convertView.setOnLongClickListener(new View.OnLongClickListener() {
//			
//			@Override
//			public boolean onLongClick(View v) {
//				scanActivity.confirmDeleteDialog(getCount()-position-1);
//				return false;
//			}
//		});
		
		if (position == 0) {
//			Animation animation = AnimationUtils.loadAnimation(scanActivity, R.anim.slide_top_to_bottom);
			convertView.startAnimation(ScanActivity.animSlideTopToBottom);	
		}

		return convertView;
	}

	static class ViewHolder {
		TextView txtName;
		TextView txtPrice;
		TextView txtQnt;
		
		TextView txtLineNo;
		TextView txtBarcode;
		TextView txtPricePerUnit;
	}
}
